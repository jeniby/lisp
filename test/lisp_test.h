#pragma once

#include <catch.hpp>
#include "./lispp/interpreter.h"
#include "lispp/utils/parsing.h"
#include "lispp/utils/exceptions.h"

struct LispTest {
    LispTest() {
        lisp = std::make_shared<Interpreter>();
    }

    void ExpectEq(std::string expression, std::string result) {
        REQUIRE(lisp->Run(expression) == result);
    }

    void ExpectNoError(std::string expression) {
        lisp->Run(expression);
    }

    void ExpectSyntaxError(std::string expression) {
        REQUIRE_THROWS_AS(lisp->Run(expression), LispSyntaxException);
    }

    void ExpectRuntimeError(std::string expression) {
        REQUIRE_THROWS_AS(lisp->Run(expression), LispRuntimeException);
    }

    void ExpectNameError(std::string expression) {
        REQUIRE_THROWS_AS(lisp->Run(expression), LispNameException);
    }

private:
    std::shared_ptr<Interpreter> lisp;
};
