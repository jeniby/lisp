#ifndef LISPP_LISPP_OPERATIONS_PREDICATE_H_
#define LISPP_LISPP_OPERATIONS_PREDICATE_H_

#include <lispp/types/environment.h>
#include <vector>

std::shared_ptr<Value> IsSymbolLispFunction(
        const std::vector<std::shared_ptr<Value>> &items);

std::shared_ptr<Value> IsPairLispFunction(
        const std::vector<std::shared_ptr<Value>> &items);

std::shared_ptr<Value> IsNullLispFunction(
        const std::vector<std::shared_ptr<Value>> &items);

std::shared_ptr<Value> IsListLispFunction(
        const std::vector<std::shared_ptr<Value>> &items);

std::shared_ptr<Value> BooleanLispFunction(
        const std::vector<std::shared_ptr<Value>> &items);

std::shared_ptr<Value> IsNumberLispFunction(
        const std::vector<std::shared_ptr<Value>> &items);

#endif
