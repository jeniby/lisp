#ifndef LISPP_LISPP_OPERATIONS_BOOLEAN_H_
#define LISPP_LISPP_OPERATIONS_BOOLEAN_H_

#include <lispp/types/environment.h>
#include <vector>


std::shared_ptr<Value> LessLispFunction(
        const std::vector<std::shared_ptr<Value>> &items);

std::shared_ptr<Value> LessEqualLispFunction(
        const std::vector<std::shared_ptr<Value>> &items);

std::shared_ptr<Value> GreaterLispFunction(
        const std::vector<std::shared_ptr<Value>> &items);

std::shared_ptr<Value> GreaterEqualLispFunction(
        const std::vector<std::shared_ptr<Value>> &items);

std::shared_ptr<Value> EqualLispFunction(
        const std::vector<std::shared_ptr<Value>> &items);



std::shared_ptr<Value> NotLispFunction(
        const std::vector<std::shared_ptr<Value>> &items);

std::shared_ptr<Value> AndLispFunction(
        const std::vector<std::shared_ptr<Value>> &items,
        const std::shared_ptr<Scope> env);

std::shared_ptr<Value> OrLispFunction(
        const std::vector<std::shared_ptr<Value>> &items,
        const std::shared_ptr<Scope> env);

#endif
