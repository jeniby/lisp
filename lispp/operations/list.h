#ifndef LISPP_LISPP_OPERATIONS_LIST_H_
#define LISPP_LISPP_OPERATIONS_LIST_H_

#include <lispp/types/environment.h>
#include <vector>

std::shared_ptr<Value> ConsLispFunction(
        const std::vector<std::shared_ptr<Value>> &items);

std::shared_ptr<Value> CarLispFunction(
        const std::vector<std::shared_ptr<Value>> &items);

std::shared_ptr<Value> CdrLispFunction(
        const std::vector<std::shared_ptr<Value>> &items);

std::shared_ptr<Value> SetCarLispFunction(
        const std::vector<std::shared_ptr<Value>> &items);

std::shared_ptr<Value> SetCdrLispFunction(
        const std::vector<std::shared_ptr<Value>> &items);

std::shared_ptr<Value> ListLispFunction(
        const std::vector<std::shared_ptr<Value>> &items);

std::shared_ptr<Value> ListTailLispFunction(
        const std::vector<std::shared_ptr<Value>> &items);

std::shared_ptr<Value> ListRefLispFunction(
        const std::vector<std::shared_ptr<Value>> &items);

#endif
