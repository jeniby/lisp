#ifndef LISPP_LISPP_OPERATIONS_ARITHMETIC_H_
#define LISPP_LISPP_OPERATIONS_ARITHMETIC_H_

#include <lispp/types/environment.h>
#include <vector>

std::shared_ptr<Value> AddLispFunction(
        const std::vector<std::shared_ptr<Value>> &items);

std::shared_ptr<Value> SubLispFunction(
        const std::vector<std::shared_ptr<Value>> &items);

std::shared_ptr<Value> MulLispFunction(
        const std::vector<std::shared_ptr<Value>> &items);

std::shared_ptr<Value> DivLispFunction(
        const std::vector<std::shared_ptr<Value>> &items);

std::shared_ptr<Value> MaxLispFunction(
        const std::vector<std::shared_ptr<Value>> &items);

std::shared_ptr<Value> MinLispFunction(
        const std::vector<std::shared_ptr<Value>> &items);

std::shared_ptr<Value> AbsLispFunction(
        const std::vector<std::shared_ptr<Value>> &items);


#endif
