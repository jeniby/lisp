#include <lispp/utils/helper.h>
#include <lispp/utils/exceptions.h>
#include <lispp/types/procedure.h>
#include "special.h"
#include "boolean.h"

std::shared_ptr<Value> QuoteOperation(std::shared_ptr<Value> next,
                                      std::shared_ptr<Scope> env) {
    return std::dynamic_pointer_cast<Pair>(next)->value_;
}

std::shared_ptr<Value> DefineOperation(std::shared_ptr<Value> next,
                                       std::shared_ptr<Scope> env) {
    auto argsVector = PairsToVector(
            std::dynamic_pointer_cast<Pair>(next), env, false);

    if (argsVector.size() != 2) {
        throw LispSyntaxException("define expect 2 arguments");
    }

    if (IsPairValue(argsVector[0])) {
        auto LambdaArgs = PairsToVector(
                std::dynamic_pointer_cast<Pair>(argsVector[0]), env, false);

        argsVector.erase(argsVector.begin());


        auto funcName = std::dynamic_pointer_cast<Symbol>(LambdaArgs[0])
                ->GetValue();

        LambdaArgs.erase(LambdaArgs.begin());

        auto lambda = std::make_shared<Lambda>(Lambda(LambdaArgs,
                                                      argsVector,
                                                      env)
                                               );

        env->Set(funcName, lambda);
    } else {
        env->Set(std::dynamic_pointer_cast<Symbol>(argsVector[0])->GetValue(),
                 argsVector[1]->Evaluate(env));
    }
    return argsVector[0];
}

std::shared_ptr<Value> SetOperation(std::shared_ptr<Value> next,
                                    std::shared_ptr<Scope> env) {
    auto argsVector = PairsToVector(std::dynamic_pointer_cast<Pair>(next),
                                    env,
                                    false);

    if (argsVector.size() != 2) {
        throw LispSyntaxException("define expect 2 arguments");
    }

    auto envForChange = env->Find(
            std::dynamic_pointer_cast<Symbol>(argsVector[0])->GetValue());

    envForChange->Set(
            std::dynamic_pointer_cast<Symbol>(argsVector[0])->GetValue(),
            argsVector[1]->Evaluate(env));

    return argsVector[0];
}

std::shared_ptr<Value> OrOperation(std::shared_ptr<Value> next,
                                   std::shared_ptr<Scope> env) {
    auto argsVector = PairsToVector(std::dynamic_pointer_cast<Pair>(next),
                                    env,
                                    false);

    return OrLispFunction(argsVector, env);
}

std::shared_ptr<Value> AndOperation(std::shared_ptr<Value> next,
                                    std::shared_ptr<Scope> env) {
    auto argsVector = PairsToVector(std::dynamic_pointer_cast<Pair>(next),
                                    env,
                                    false);

    return AndLispFunction(argsVector, env);
}

std::shared_ptr<Value> IfOperation(std::shared_ptr<Value> next,
                                   std::shared_ptr<Scope> env) {
    auto argsVector = PairsToVector(std::dynamic_pointer_cast<Pair>(next),
                                    env,
                                    false);

    if (argsVector.size() < 1 || argsVector.size() > 3) {
        throw LispSyntaxException("if expect 1 or 2 or 3 arguments");
    }

    if (IsFalseValue(argsVector[0]->Evaluate(env))) {
        return (argsVector.size() < 3) ?
               std::static_pointer_cast<Value>(std::make_shared<Pair>(Pair()))
                                       : argsVector[2]->Evaluate(env);
    } else {
        return argsVector[1]->Evaluate(env);
    }
}

std::shared_ptr<Value> LambdaOperations(std::shared_ptr<Value> next,
                                        std::shared_ptr<Scope> env) {
    auto argsVector = PairsToVector(std::dynamic_pointer_cast<Pair>(next),
                                    env,
                                    false);

    if (argsVector.size() < 2) {
        throw LispSyntaxException("lambda error");
    }

    auto argumentsForLambda = PairsToVector(
            std::dynamic_pointer_cast<Pair>(argsVector[0]),
            env,
            false);

    argsVector.erase(argsVector.begin());

    auto lambda = std::make_shared<Lambda>(Lambda(argumentsForLambda,
                                                  argsVector,
                                                  env));

    return std::static_pointer_cast<Value>(lambda);
}
