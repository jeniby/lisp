#ifndef LISPP_LISPP_OPERATIONS_SPECIAL_H_
#define LISPP_LISPP_OPERATIONS_SPECIAL_H_

#include <lispp/types/environment.h>

std::shared_ptr<Value> QuoteOperation(std::shared_ptr<Value> next,
                                      std::shared_ptr<Scope> env);

std::shared_ptr<Value> DefineOperation(std::shared_ptr<Value> next,
                                       std::shared_ptr<Scope> env);

std::shared_ptr<Value> SetOperation(std::shared_ptr<Value> next,
                                    std::shared_ptr<Scope> env);

std::shared_ptr<Value> OrOperation(std::shared_ptr<Value> next,
                                   std::shared_ptr<Scope> env);

std::shared_ptr<Value> AndOperation(std::shared_ptr<Value> next,
                                    std::shared_ptr<Scope> env);

std::shared_ptr<Value> IfOperation(std::shared_ptr<Value> next,
                                   std::shared_ptr<Scope> env);

std::shared_ptr<Value> LambdaOperations(std::shared_ptr<Value> next,
                                        std::shared_ptr<Scope> env);
#endif
