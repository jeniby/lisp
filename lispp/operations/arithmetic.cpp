#include <lispp/types/simple_type.h>
#include <lispp/utils/helper.h>
#include <lispp/utils/exceptions.h>
#include "lispp/operations/arithmetic.h"
#include <vector>


std::shared_ptr<Value> AddLispFunction(
        const std::vector<std::shared_ptr<Value>> &items) {
    auto result = std::make_shared<Number>(Number(0));

    for (const auto& item : items) {
        if (!IsNumberValue(item)) {
            throw LispRuntimeException("not number");
        }
        result->SetValue(result->GetValue() +
                         std::dynamic_pointer_cast<Number>(item)->GetValue());
    }

    return std::static_pointer_cast<Value>(result);
}

std::shared_ptr<Value> SubLispFunction(
        const std::vector<std::shared_ptr<Value>> &items) {
    if (items.size() == 0) {
        throw LispRuntimeException("expect arguments");
    }

    if (!IsNumberValue(items[0])) {
        throw LispRuntimeException("not number");
    }

    auto result = std::dynamic_pointer_cast<Number>(items[0]);

    for (size_t i = 1; i < items.size(); ++i) {
        if (!IsNumberValue(items[i])) {
            throw LispRuntimeException("not number");
        }
        result->SetValue(
                result->GetValue() -
                std::dynamic_pointer_cast<Number>(items[i])->GetValue());
    }
    return std::static_pointer_cast<Value>(result);
}

std::shared_ptr<Value> MulLispFunction(
        const std::vector<std::shared_ptr<Value>> &items) {
    auto result = std::make_shared<Number>(Number(1));

    for (const auto& item : items) {
        if (!IsNumberValue(item)) {
            throw LispRuntimeException("not number");
        }
        result->SetValue(
                result->GetValue() *
                std::dynamic_pointer_cast<Number>(item)->GetValue());
    }
    return std::static_pointer_cast<Value>(result);
}

std::shared_ptr<Value> DivLispFunction(
        const std::vector<std::shared_ptr<Value>> &items) {
    if (items.size() == 0) {
        throw LispRuntimeException("expect arguments");
    }

    if (!IsNumberValue(items[0])) {
        throw LispRuntimeException("not number");
    }

    auto result = std::dynamic_pointer_cast<Number>(items[0]);

    for (size_t i = 1; i < items.size(); ++i) {
        if (!IsNumberValue(items[i])) {
            throw LispRuntimeException("not number");
        }
        result->SetValue(
                result->GetValue() /
                std::dynamic_pointer_cast<Number>(items[i])->GetValue());
    }
    return result;
}

std::shared_ptr<Value> IsNumberLispFunction(
        const std::vector<std::shared_ptr<Value>> &items) {
    if (items.size() != 1) {
        throw LispRuntimeException("invalid number of arguments");
    }
    if (IsNumberValue(items[0])) {
        auto trueLisp = std::make_shared<True>(True());

        return std::static_pointer_cast<Value>(trueLisp);
    } else {
        auto falseLisp =  std::make_shared<False>(False());

        return std::static_pointer_cast<Value>(falseLisp);
    }
}

std::shared_ptr<Value> MaxLispFunction(
        const std::vector<std::shared_ptr<Value>> &items) {
    if (items.size() == 0) {
        throw LispRuntimeException("expect arguments");
    }

    if (!IsNumberValue(items[0])) {
        throw LispRuntimeException("not number");
    }

    auto max = std::dynamic_pointer_cast<Number>(items[0]);

    for (size_t i = 1; i < items.size(); ++i) {

        if (!IsNumberValue(items[i])) {
            throw LispRuntimeException("not number");
        }
        auto currentNumber = std::dynamic_pointer_cast<Number>(items[i]);
        if (max->GetValue() < currentNumber->GetValue()) {
            max = currentNumber;
        }
    }
    return max;
}

std::shared_ptr<Value> MinLispFunction(
        const std::vector<std::shared_ptr<Value>> &items) {
        if (items.size() == 0) {
            throw LispRuntimeException("expect arguments");
        }

        if (!IsNumberValue(items[0])) {
            throw LispRuntimeException("not number");
        }

        auto min = std::dynamic_pointer_cast<Number>(items[0]);

        for (size_t i = 1; i < items.size(); ++i) {

            if (!IsNumberValue(items[i])) {
                throw LispRuntimeException("not number");
            }

            auto currentNumber = std::dynamic_pointer_cast<Number>(items[i]);
            if (min->GetValue() > currentNumber->GetValue()) {
                min = currentNumber;
            }
        }
        return min;
    }

std::shared_ptr<Value> AbsLispFunction(
        const std::vector<std::shared_ptr<Value>> &items) {
    if (items.size() == 1) {

        if (!IsNumberValue(items[0])) {
            throw LispRuntimeException("not number");
        }

        auto result = std::dynamic_pointer_cast<Number>(items[0]);

       if (result->GetValue() < 0) {
           result->SetValue(result->GetValue() * -1);
       }

       return result;
    } else {
       throw LispRuntimeException("invalid number of arguments");
    }
}
