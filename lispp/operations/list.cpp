#include <lispp/utils/exceptions.h>
#include <lispp/utils/helper.h>
#include "lispp/operations/list.h"
#include <vector>

std::shared_ptr<Value> ConsLispFunction(
        const std::vector<std::shared_ptr<Value>> &items) {
    if (items.size() != 2) {
        throw LispRuntimeException("Cons expected 2 arguments");
    }

    auto resultPair = std::make_shared<Pair>(Pair());

    resultPair->value_ = items[0];
    resultPair->next_ = items[1];

    return resultPair;
}

std::shared_ptr<Value> CarLispFunction(
        const std::vector<std::shared_ptr<Value>> &items) {

    return std::dynamic_pointer_cast<Pair>(items[0])->value_;
}

std::shared_ptr<Value> CdrLispFunction(
        const std::vector<std::shared_ptr<Value>> &items) {
    return std::dynamic_pointer_cast<Pair>(items[0])->next_;
}

std::shared_ptr<Value> SetCarLispFunction(
        const std::vector<std::shared_ptr<Value>> &items) {
    std::dynamic_pointer_cast<Pair>(items[0])->value_ = items[1];
    return items[0];
}

std::shared_ptr<Value> SetCdrLispFunction(
        const std::vector<std::shared_ptr<Value>> &items) {
    std::dynamic_pointer_cast<Pair>(items[0])->next_ = items[1];
    return items[0];
}

std::shared_ptr<Value> ListLispFunction(
        const std::vector<std::shared_ptr<Value>> &items) {
    auto list = std::make_shared<Pair>(Pair());
    auto currentPair = list;

    for (const auto & item : items) {
        currentPair->value_ = item;

        currentPair->next_ = std::make_shared<Pair>(Pair());

        currentPair = std::dynamic_pointer_cast<Pair>(currentPair->next_);
    }
    return list;
}

std::shared_ptr<Value> ListRefLispFunction(
        const std::vector<std::shared_ptr<Value>> &items) {
    auto list = PairsToVector(std::dynamic_pointer_cast<Pair>(items[0]),
                              std::make_shared<Scope>(Scope()), false);

    auto index = std::dynamic_pointer_cast<Number>(items[1])->GetValue();

    if (list.size() <= index) {
        throw LispRuntimeException("out of range");
    }

    return list[index];
}

std::shared_ptr<Value> ListTailLispFunction(
        const std::vector<std::shared_ptr<Value>> &items) {
    auto index = std::dynamic_pointer_cast<Number>(items[1])->GetValue();

    auto currentPair = std::dynamic_pointer_cast<Pair>(items[0]);

    int count = 0;

    while (index != count && currentPair->value_) {
        count++;

        currentPair = std::dynamic_pointer_cast<Pair>(currentPair->next_);
    }

    if (count < index) {

        throw LispRuntimeException("out of range");
    }

    return currentPair;
}
