#include <lispp/utils/exceptions.h>
#include <lispp/utils/helper.h>
#include "lispp/operations/predicate.h"


std::shared_ptr<Value> BooleanLispFunction(
        const std::vector<std::shared_ptr<Value>> &items) {
    if (items.size() != 1) {
        throw LispRuntimeException("invalid number of arguments");
    }

    if (IsTrueValue(items[0]) || IsFalseValue(items[0])) {
        auto trueLisp = std::make_shared<True>(True());
        return std::static_pointer_cast<Value>(trueLisp);
    } else {
        auto falseLisp =  std::make_shared<False>(False());
        return std::static_pointer_cast<Value>(falseLisp);
    }
}

std::shared_ptr<Value> IsSymbolLispFunction(
        const std::vector<std::shared_ptr<Value>> &items) {
    if (items.size() != 1) {
        throw LispRuntimeException("invalid number of arguments");
    }
    if (IsSymbolValue(items[0])) {
        auto trueLisp = std::make_shared<True>(True());

        return std::static_pointer_cast<Value>(trueLisp);
    } else {
        auto falseLisp =  std::make_shared<False>(False());

        return std::static_pointer_cast<Value>(falseLisp);
    }
}

std::shared_ptr<Value> IsPairLispFunction(
        const std::vector<std::shared_ptr<Value>> &items) {
    if (PairsToVector(std::dynamic_pointer_cast<Pair>(items[0]),
                      std::make_shared<Scope>(Scope()), false).size() == 2) {
        auto trueLisp = std::make_shared<True>(True());

        return std::static_pointer_cast<Value>(trueLisp);
    } else {
        auto falseLisp =  std::make_shared<False>(False());

        return std::static_pointer_cast<Value>(falseLisp);
    }
}

std::shared_ptr<Value> IsNullLispFunction(
        const std::vector<std::shared_ptr<Value>> &items) {
    if (PairsToVector(std::dynamic_pointer_cast<Pair>(items[0]),
                      std::make_shared<Scope>(Scope()), false).size() == 0) {
        auto trueLisp = std::make_shared<True>(True());

        return std::static_pointer_cast<Value>(trueLisp);
    } else {
        auto falseLisp = std::make_shared<False>(False());

        return std::static_pointer_cast<Value>(falseLisp);
    }
}

std::shared_ptr<Value> IsListLispFunction(
        const std::vector<std::shared_ptr<Value>> &items) {
    auto trueLisp = std::make_shared<True>(True());

    auto currentPair = std::dynamic_pointer_cast<Pair>(items[0]);

    while (currentPair->value_) {
        if (IsNumberValue(currentPair->next_)) {
            auto falseLisp = std::make_shared<False>(False());
            return std::static_pointer_cast<Value>(falseLisp);
        }
        currentPair = std::dynamic_pointer_cast<Pair>(currentPair->next_);
    }
    return trueLisp;
}