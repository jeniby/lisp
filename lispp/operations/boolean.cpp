#include <lispp/utils/helper.h>
#include <lispp/utils/exceptions.h>
#include "lispp/operations/boolean.h"
#include <iostream>
#include <vector>

std::shared_ptr<Value> LessLispFunction(
        const std::vector<std::shared_ptr<Value>> &items) {
    if (items.size() == 0) {
        auto falseLisp =  std::make_shared<True>(True());
        return std::static_pointer_cast<Value>(falseLisp);
    }

    if (!IsNumberValue(items[0])) {
        throw LispRuntimeException("not number");
    }

    auto result = std::dynamic_pointer_cast<Number>(items[0]);

    for (size_t i = 1; i < items.size(); ++i) {

        if (!IsNumberValue(items[i])) {
            throw LispRuntimeException("not number");
        }

        if (result->GetValue() >=
                std::dynamic_pointer_cast<Number>(items[i])->GetValue()) {
            auto falseLisp =  std::make_shared<False>(False());
            return std::static_pointer_cast<Value>(falseLisp);
        }
    }

    auto trueLisp = std::make_shared<True>(True());

    return std::static_pointer_cast<Value>(trueLisp);
}

std::shared_ptr<Value> LessEqualLispFunction(
        const std::vector<std::shared_ptr<Value>> &items) {
    if (items.size() == 0) {
        auto falseLisp =  std::make_shared<True>(True());
        return std::static_pointer_cast<Value>(falseLisp);
    }

    if (!IsNumberValue(items[0])) {
        throw LispRuntimeException("not number");
    }

    auto result = std::dynamic_pointer_cast<Number>(items[0]);

    for (size_t i = 1; i < items.size(); ++i) {

        if (!IsNumberValue(items[i])) {
            throw LispRuntimeException("not number");
        }

        if (result->GetValue() >
                std::dynamic_pointer_cast<Number>(items[i])->GetValue()) {
            auto falseLisp =  std::make_shared<False>(False());
            return std::static_pointer_cast<Value>(falseLisp);
        }
    }
    auto trueLisp = std::make_shared<True>(True());

    return std::static_pointer_cast<Value>(trueLisp);
}

std::shared_ptr<Value> GreaterLispFunction(
        const std::vector<std::shared_ptr<Value>> &items) {
    if (items.size() == 0) {
        auto falseLisp =  std::make_shared<True>(True());
        return std::static_pointer_cast<Value>(falseLisp);
    }

    if (!IsNumberValue(items[0])) {
        throw LispRuntimeException("not number");
    }

    auto result = std::dynamic_pointer_cast<Number>(items[0]);

    for (size_t i = 1; i < items.size(); ++i) {

        if (!IsNumberValue(items[i])) {
            throw LispRuntimeException("not number");
        }

        if (result->GetValue() <=
                std::dynamic_pointer_cast<Number>(items[i])->GetValue()) {
            auto falseLisp =  std::make_shared<False>(False());
            return std::static_pointer_cast<Value>(falseLisp);
        }
    }
    auto trueLisp = std::make_shared<True>(True());

    return std::static_pointer_cast<Value>(trueLisp);
}

std::shared_ptr<Value> GreaterEqualLispFunction(
        const std::vector<std::shared_ptr<Value>> &items)  {
    if (items.size() == 0) {
        auto falseLisp =  std::make_shared<True>(True());
        return std::static_pointer_cast<Value>(falseLisp);
    }

    if (!IsNumberValue(items[0])) {
        throw LispRuntimeException("not number");
    }

    auto result = std::dynamic_pointer_cast<Number>(items[0]);

    for (size_t i = 1; i < items.size(); ++i) {

        if (!IsNumberValue(items[i])) {
            throw LispRuntimeException("not number");
        }

        if (result->GetValue() <
                std::dynamic_pointer_cast<Number>(items[i])->GetValue()) {
            auto falseLisp =  std::make_shared<False>(False());
            return std::static_pointer_cast<Value>(falseLisp);
        }
    }
    auto trueLisp = std::make_shared<True>(True());

    return std::static_pointer_cast<Value>(trueLisp);
}

std::shared_ptr<Value> EqualLispFunction(
        const std::vector<std::shared_ptr<Value>> &items)  {
    if (items.size() == 0) {
        auto falseLisp =  std::make_shared<True>(True());
        return std::static_pointer_cast<Value>(falseLisp);
    }

    if (!IsNumberValue(items[0])) {
        throw LispRuntimeException("not number");
    }

    auto result = std::dynamic_pointer_cast<Number>(items[0]);

    for (size_t i = 1; i < items.size(); ++i) {
        if (!IsNumberValue(items[i])) {
            throw LispRuntimeException("not number");
        }

        if (result->GetValue() !=
                std::dynamic_pointer_cast<Number>(items[i])->GetValue()) {
            auto falseLisp =  std::make_shared<False>(False());
            return std::static_pointer_cast<Value>(falseLisp);
        }
    }
    auto trueLisp = std::make_shared<True>(True());

    return std::static_pointer_cast<Value>(trueLisp);
}

std::shared_ptr<Value> BooleanLispFunction(
        const std::vector<std::shared_ptr<Value>> &items) {
    if (items.size() != 1) {
        throw LispRuntimeException("invalid number of arguments");
    }

    if (IsTrueValue(items[0]) || IsFalseValue(items[0])) {
        auto trueLisp = std::make_shared<True>(True());
        return std::static_pointer_cast<Value>(trueLisp);
    } else {
        auto falseLisp =  std::make_shared<False>(False());
        return std::static_pointer_cast<Value>(falseLisp);
    }
}

std::shared_ptr<Value> NotLispFunction(
        const std::vector<std::shared_ptr<Value>> &items) {
    if (items.size() != 1) {
        throw LispRuntimeException("invalid number of arguments");
    }

    if (IsFalseValue(items[0])) {
        auto trueLisp = std::make_shared<True>(True());
        return std::static_pointer_cast<Value>(trueLisp);
    } else {
        auto falseLisp =  std::make_shared<False>(False());
        return std::static_pointer_cast<Value>(falseLisp);
    }
}

std::shared_ptr<Value> AndLispFunction(
        const std::vector<std::shared_ptr<Value>> &items,
        const std::shared_ptr<Scope> env) {
    auto result = std::static_pointer_cast<Value>(
            std::make_shared<True>(True()));

    for (const auto& item : items) {
        auto evalValue = item->Evaluate(env);

        if (IsFalseValue(evalValue)) {
            return evalValue;
        }

        result = evalValue;
    }
    return result;
}

std::shared_ptr<Value> OrLispFunction(
        const std::vector<std::shared_ptr<Value>> &items,
        const std::shared_ptr<Scope> env) {
    auto result = std::static_pointer_cast<Value>(
            std::make_shared<False>(False()));

    for (const auto& item : items) {
        auto evalValue = item->Evaluate(env);

        if (IsTrueValue(evalValue)) {
            return evalValue;
        }

        result = evalValue;
    }
    return result;
}

std::shared_ptr<Value> IsSymbolLispFunction(
        const std::vector<std::shared_ptr<Value>> &items) {
    if (items.size() != 1) {
        throw LispRuntimeException("invalid number of arguments");
    }
    if (IsSymbolValue(items[0])) {
        auto trueLisp = std::make_shared<True>(True());

        return std::static_pointer_cast<Value>(trueLisp);
    } else {
        auto falseLisp =  std::make_shared<False>(False());

        return std::static_pointer_cast<Value>(falseLisp);
    }
}

std::shared_ptr<Value> IsPairLispFunction(
        const std::vector<std::shared_ptr<Value>> &items) {
    if (PairsToVector(std::dynamic_pointer_cast<Pair>(items[0]),
                      std::make_shared<Scope>(Scope()), false).size() == 2) {
        auto trueLisp = std::make_shared<True>(True());

        return std::static_pointer_cast<Value>(trueLisp);
    } else {
        auto falseLisp =  std::make_shared<False>(False());

        return std::static_pointer_cast<Value>(falseLisp);
    }
}

std::shared_ptr<Value> IsNullLispFunction(
        const std::vector<std::shared_ptr<Value>> &items) {
    if (PairsToVector(std::dynamic_pointer_cast<Pair>(items[0]),
                      std::make_shared<Scope>(Scope()), false).size() == 0) {
        auto trueLisp = std::make_shared<True>(True());

        return std::static_pointer_cast<Value>(trueLisp);
    } else {
        auto falseLisp = std::make_shared<False>(False());

        return std::static_pointer_cast<Value>(falseLisp);
    }
}

std::shared_ptr<Value> IsListLispFunction(
        const std::vector<std::shared_ptr<Value>> &items) {
    auto trueLisp = std::make_shared<True>(True());

    auto currentPair = std::dynamic_pointer_cast<Pair>(items[0]);

    while (currentPair->value_) {
        if (IsNumberValue(currentPair->next_)) {
            auto falseLisp = std::make_shared<False>(False());
            return std::static_pointer_cast<Value>(falseLisp);
        }
        currentPair = std::dynamic_pointer_cast<Pair>(currentPair->next_);
    }
    return trueLisp;
}
