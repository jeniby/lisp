#include <sstream>
#include "lispp/types/simple_type.h"
#include <string>

std::shared_ptr<Value> Number::Evaluate(const std::shared_ptr<Scope>& env) {
    return shared_from_this();
}

std::string Number::ToString() {
    std::string strNumber;
    std::stringstream strStream;
    strStream << value_;
    strStream >> strNumber;
    return strNumber;
}

int64_t Number::GetValue() {
    return  value_;
}

void Number::SetValue(int64_t value) {
    value_ = value;
}


std::shared_ptr<Value> Symbol::Evaluate(const std::shared_ptr<Scope>& env) {
    return env->Get(value_);
}

std::string Symbol::ToString() {
    return value_;
}

std::string Symbol::GetValue() {
    return  value_;
}


std::shared_ptr<Value> Nil::Evaluate(const std::shared_ptr<Scope>& env) {
    return shared_from_this();
}

std::string Nil::ToString() {
    return value_;
}


std::shared_ptr<Value> False::Evaluate(const std::shared_ptr<Scope>& env) {
    return shared_from_this();
}

std::string False::ToString() {
    return value_;
}


std::shared_ptr<Value> True::Evaluate(const std::shared_ptr<Scope>& env) {
    return shared_from_this();
}

std::string True::ToString() {
    return value_;
}
