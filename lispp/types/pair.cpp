#include <lispp/utils/helper.h>
#include <lispp/utils/exceptions.h>
#include "lispp/types/procedure.h"
#include "lispp/types/pair.h"
#include <string>

std::shared_ptr<Value> Pair::Evaluate(const std::shared_ptr<Scope>& env) {

    if (IsNumberValue(value_) || !value_) {
        throw LispRuntimeException("not a function");
    }

    if (IsSymbolValue(value_)) {
        auto operationName = std::dynamic_pointer_cast<Symbol>(value_)
                ->GetValue();

        auto specialFunc = env->GetSpecialOperations(operationName);

        if (specialFunc) {
            return specialFunc(next_, env);
        }
    }

    auto func = std::dynamic_pointer_cast<IProcedure>(value_->Evaluate(env));
    auto argsVector = PairsToVector(std::dynamic_pointer_cast<Pair>(next_),
                                    env,
                                    true);

    return func->EvaluateWithArguments(env, argsVector);
}

std::string Pair::ToString() {
    if (!value_) {
        return "()";
    }

    std::string result = "(" + value_->ToString() + ' ';

    if (IsNumberValue(next_)) {
        result += ". " + next_->ToString() + ")";
        return result;
    }

    auto currentPair = std::dynamic_pointer_cast<Pair>(next_);

    while (currentPair && currentPair->value_) {
        result += currentPair->value_->ToString() + ' ';
        if (IsNumberValue(currentPair->next_)) {
            result += ". " + currentPair->next_->ToString();
            break;
        } else {
            currentPair = std::dynamic_pointer_cast<Pair>(currentPair->next_);
        }
    }

    if (*(--result.end()) == ' ') {
        result.erase(--result.end());
    }
    return result + ")";
}
