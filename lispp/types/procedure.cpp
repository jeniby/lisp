#include <lispp/utils/helper.h>
#include "lispp/types/procedure.h"
#include <string>
#include <vector>

std::shared_ptr<Value> IProcedure::Evaluate(
        const std::shared_ptr<Scope>& env) {}

std::string IProcedure::ToString() {}

std::shared_ptr<Value> IProcedure::EvaluateWithArguments(
        const std::shared_ptr<Scope>& env,
        std::vector<std::shared_ptr<Value>> args) {}


std::shared_ptr<Value> Function::EvaluateWithArguments(
        const std::shared_ptr<Scope>& env,
        std::vector<std::shared_ptr<Value>> args) {
    return func_(args);
}


std::shared_ptr<Value> Lambda::EvaluateWithArguments(
        const std::shared_ptr<Scope>& env,
        std::vector<std::shared_ptr<Value>> args) {
    auto newEnv = std::make_shared<Scope>(Scope(lambdaEnv_, params_, args));

    std::shared_ptr<Value> result;

    for (const auto& value: body_) {
        result = value->Evaluate(newEnv);
    }

    return result;
}
