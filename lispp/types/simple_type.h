#ifndef LISPP_LISPP_TYPES_SIMPLE_TYPE_H_
#define LISPP_LISPP_TYPES_SIMPLE_TYPE_H_

#include "lispp/types/environment.h"
#include <string>

class Number : public Value, public std::enable_shared_from_this<Value> {
 public:
    Number(int64_t value)
            : value_(value){}

    std::shared_ptr<Value> Evaluate(const std::shared_ptr<Scope>& env);

    int64_t GetValue();

    void SetValue(int64_t value);

    std::string ToString();
 private:
    int64_t value_;
};


class Symbol : public Value, public std::enable_shared_from_this<Value> {
 public:
    Symbol(std::string value)
            : value_(value) {}

    std::shared_ptr<Value> Evaluate(const std::shared_ptr<Scope>& env);

    std::string GetValue();

    std::string ToString();
 private:
    std::string value_;
};


class Nil : public Value, public std::enable_shared_from_this<Value> {
 public:
    Nil(): value_("nil") {}

    std::shared_ptr<Value> Evaluate(const std::shared_ptr<Scope>& env);

    std::string ToString();
 private:
    std::string value_;
};

class True : public Value, public std::enable_shared_from_this<Value> {
 public:
    True(): value_("#t") {}

    std::shared_ptr<Value> Evaluate(const std::shared_ptr<Scope>& env);

    std::string ToString();
 private:
    std::string value_;
};

class False : public Value, public std::enable_shared_from_this<Value> {
 public:
    False(): value_("#f") {}

    std::shared_ptr<Value> Evaluate(const std::shared_ptr<Scope>& env);

    std::string ToString();
 private:
    std::string value_;
};
#endif
