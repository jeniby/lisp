#include <lispp/operations/arithmetic.h>
#include <lispp/operations/boolean.h>
#include <lispp/utils/exceptions.h>
#include <lispp/operations/list.h>
#include <lispp/operations/special.h>
#include <lispp/operations/predicate.h>
#include "lispp/types/environment.h"
#include "lispp/types/simple_type.h"
#include "lispp/types/procedure.h"
#include <vector>
#include <string>

Scope::Scope() {}

Scope::Scope(std::shared_ptr<Scope> other,
             std::vector<std::shared_ptr<Value>> params,
             std::vector<std::shared_ptr<Value>> values) : other_(other){

    for (int i = 0; i < params.size(); ++i) {
        std::string key = std::dynamic_pointer_cast<Symbol>(params[i])
                ->GetValue();

        env_[key] = values[i];
    }
}

std::shared_ptr<Value> Scope::Get(const std::string& key) const {
    auto search = env_.find(key);

    if (search != env_.end()) {
        return (*search).second;;
    }

    if (other_) {
        return other_->Get(key);
    }

    throw LispNameException("variable not implemented");
}

void Scope::Set(const std::string& key, const std::shared_ptr<Value>& value) {
    env_[key] = value;
}

std::shared_ptr<Scope> Scope::Find(const std::string& key) {
    auto search = env_.find(key);

    if (search != env_.end()) {
        return shared_from_this();
    }

    if (other_) {
        return other_->Find(key);
    }

    throw LispNameException("variable not implemented");
}

std::function<
        std::shared_ptr<Value>(std::shared_ptr<Value>,
                               std::shared_ptr<Scope>)
            > Scope::GetSpecialOperations(const std::string& key) {
    auto search = specialOperations.find(key);

    if (search != specialOperations.end()) {
        return (*search).second;
    }

    if (other_) {
        return other_->GetSpecialOperations(key);
    }

    return nullptr;
}

void Scope::SetGlobalOperations() {
    env_["+"] = std::make_shared<Function>(&AddLispFunction);
    env_["-"] = std::make_shared<Function>(&SubLispFunction);
    env_["*"] = std::make_shared<Function>(&MulLispFunction);
    env_["/"] = std::make_shared<Function>(&DivLispFunction);

    env_["number?"] = std::make_shared<Function>(&IsNumberLispFunction);
    env_["symbol?"] = std::make_shared<Function>(&IsSymbolLispFunction);
    env_["pair?"] = std::make_shared<Function>(&IsPairLispFunction);
    env_["null?"] = std::make_shared<Function>(&IsNullLispFunction);
    env_["list?"] = std::make_shared<Function>(&IsListLispFunction);
    env_["boolean?"] = std::make_shared<Function>(&BooleanLispFunction);

    env_["<"] = std::make_shared<Function>(&LessLispFunction);
    env_["<="] = std::make_shared<Function>(&LessEqualLispFunction);
    env_[">"] = std::make_shared<Function>(&GreaterLispFunction);
    env_[">="] = std::make_shared<Function>(&GreaterEqualLispFunction);
    env_["="] = std::make_shared<Function>(&EqualLispFunction);

    env_["max"] = std::make_shared<Function>(&MaxLispFunction);
    env_["min"] = std::make_shared<Function>(&MinLispFunction);
    env_["abs"] = std::make_shared<Function>(&AbsLispFunction);

    env_["not"] = std::make_shared<Function>(&NotLispFunction);
    env_["cons"] = std::make_shared<Function>(&ConsLispFunction);
    env_["car"] = std::make_shared<Function>(&CarLispFunction);
    env_["set-car!"] = std::make_shared<Function>(&SetCarLispFunction);
    env_["cdr"] = std::make_shared<Function>(&CdrLispFunction);
    env_["set-cdr!"] = std::make_shared<Function>(&SetCdrLispFunction);
    env_["list"] = std::make_shared<Function>(&ListLispFunction);
    env_["list-ref"] = std::make_shared<Function>(&ListRefLispFunction);
    env_["list-tail"] = std::make_shared<Function>(&ListTailLispFunction);

    env_["nil"] = std::make_shared<Nil>(Nil());
    env_["#t"] = std::make_shared<True>(True());
    env_["#f"] = std::make_shared<False>(False());

    specialOperations["quote"] = &QuoteOperation;
    specialOperations["define"] = &DefineOperation;
    specialOperations["set!"] = &SetOperation;
    specialOperations["or"] = &OrOperation;
    specialOperations["and"] = &AndOperation;
    specialOperations["if"] = &IfOperation;
    specialOperations["lambda"] = &LambdaOperations;
}

std::shared_ptr<Value> Value::Evaluate(const std::shared_ptr<Scope>& env) {}

std::string Value::ToString() {}
