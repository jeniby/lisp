#ifndef LISPP_LISPP_TYPES_PAIR_H_
#define LISPP_LISPP_TYPES_PAIR_H_

#include "lispp/types/environment.h"
#include <string>

class Pair : public Value, public std::enable_shared_from_this<Value> {
 public:
    std::shared_ptr<Value> value_;
    std::shared_ptr<Value> next_;

    Pair() {}

    Pair(std::shared_ptr<Value> value)
            : value_(value){}

    std::shared_ptr<Value> Evaluate(const std::shared_ptr<Scope>& env);

    std::string ToString();

 private:
};

#endif
