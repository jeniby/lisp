#ifndef LISPP_LISPP_TYPES_ENVIRONMENT_H_
#define LISPP_LISPP_TYPES_ENVIRONMENT_H_

#include <memory>
#include <map>
#include <string>
#include <vector>

class Scope;

class Value {
 public:
    Value() {}

    virtual std::shared_ptr<Value> Evaluate(const std::shared_ptr<Scope>& env);

    virtual std::string ToString();
};

class Scope: public std::enable_shared_from_this<Scope>{
 public:
    std::map<std::string, std::shared_ptr<Value>> env_;

    Scope();

    Scope(std::shared_ptr<Scope> other,
          std::vector<std::shared_ptr<Value>> params,
          std::vector<std::shared_ptr<Value>> values);

    void SetGlobalOperations();

    std::shared_ptr<Value> Get(const std::string& key) const;

    void Set(const std::string& key, const std::shared_ptr<Value>& value);

    std::shared_ptr<Scope> Find(const std::string& key);

    std::function<std::shared_ptr<Value>(
            std::shared_ptr<Value>,
            std::shared_ptr<Scope>)> GetSpecialOperations(
                                        const std::string& key);

 private:
    std::map<std::string,
            std::function<std::shared_ptr<Value>(std::shared_ptr<Value>,
                                                 std::shared_ptr<Scope>)>
            > specialOperations;

    std::shared_ptr<Scope> other_;
};

#endif
