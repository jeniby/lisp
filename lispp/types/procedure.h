#ifndef LISPP_LISPP_TYPES_PROCEDURE_H_
#define LISPP_LISPP_TYPES_PROCEDURE_H_

#include "lispp/types/environment.h"
#include <vector>
#include <string>


class IProcedure : public Value, public std::enable_shared_from_this<Value>{
 public:
    IProcedure() {}
    std::shared_ptr<Value> Evaluate(const std::shared_ptr<Scope>& env);
    std::string ToString();
    virtual std::shared_ptr<Value> EvaluateWithArguments(
            const std::shared_ptr<Scope>& env,
            std::vector<std::shared_ptr<Value>> args);
};


class Function : public IProcedure,
                 public std::enable_shared_from_this<IProcedure> {
 public:
    Function(std::function<std::shared_ptr<Value>(
            const std::vector<std::shared_ptr<Value>>&)> func)
            : func_(func) {}

    std::shared_ptr<Value> EvaluateWithArguments(
            const std::shared_ptr<Scope>& env,
            std::vector<std::shared_ptr<Value>> args);

 private:
    std::function<std::shared_ptr<Value>(
            const std::vector<std::shared_ptr<Value>>&)> func_;
};

class Lambda : public IProcedure,
               public std::enable_shared_from_this<IProcedure> {
 public:
    Lambda(const std::vector<std::shared_ptr<Value>>& params,
           const std::vector<std::shared_ptr<Value>>& body,
           const std::shared_ptr<Scope>& env)
            : body_(body), params_(params), lambdaEnv_(env) {}

    std::shared_ptr<Value> EvaluateWithArguments(
            const std::shared_ptr<Scope>& env,
            std::vector<std::shared_ptr<Value>> args);

 private:
    std::vector<std::shared_ptr<Value>> body_;
    std::vector<std::shared_ptr<Value>> params_;
    std::shared_ptr<Scope> lambdaEnv_;
};

#endif
