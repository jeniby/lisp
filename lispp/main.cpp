#include <iostream>
#include "interpreter.h"
#include "lispp/utils/parsing.h"
#include "lispp/utils/exceptions.h"

int main() {
    Interpreter lisp = Interpreter();
//    lisp.Run("(define x 1)");
//    lisp.Run(R"(
//        (define range
//          (lambda (x)
//            (lambda ()
//              (set! x (+ x 1))
//              x)))
//                    )");
//    lisp.Run("(define my-range (range 10))");
    std::cout << lisp.Run("(quote (1 2))");
    return 0;
}
