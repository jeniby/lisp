#pragma once

#ifndef LISPP_INTERPRETER_H
#define LISPP_INTERPRETER_H

#include <list>
#include "lispp/types/environment.h"

class Interpreter {
 public:
    Interpreter();

    std::string Run(const std::string& expression);

    ~Interpreter();
 private:
    std::shared_ptr<Scope> env_;
};

#endif