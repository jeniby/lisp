#include <lispp/types/simple_type.h>
#include <lispp/types/pair.h>
#include "lispp/utils/parsing.h"
#include "lispp/utils/exceptions.h"
#include <list>
#include <string>

std::list<std::string> Split(const std::string& input) {
    std::list<std::string> tokens;
    std::string::const_iterator begin = input.begin();

    while (begin != input.end()) {
        if (*begin == ' ' || *begin == '\n') {
            ++begin;
            continue;
        }

        if (*begin == '(' || *begin == ')' || *begin == '\'') {
            tokens.push_back(std::string(begin, begin + 1));
            ++begin;
        } else {
            std::string::const_iterator beginBuffer = begin;

            while (beginBuffer != input.end()
                   && *beginBuffer != '('
                   && *beginBuffer != '\''
                   && *beginBuffer != ')'
                   && *beginBuffer != ' '
                   && *beginBuffer != '\n') {
                ++beginBuffer;
            }
            tokens.push_back(std::string(begin, beginBuffer));
            begin = beginBuffer;
        }
    }
    return tokens;
}

bool IsNumber(std::string* token) {
    if (atoi(token->c_str()) == 0 && *token != "0") {
        return false;
    } else {
        if ((*token)[0] == '+') {
            token->erase(token->begin());
        }
        return true;
    }
}

std::shared_ptr<Value> BuildSyntaxTree(std::list<std::string>* tokens) {
    std::string token = tokens->front();
    tokens->pop_front();

    if (token == "'") {
        auto list = std::make_shared<Pair>(Pair());
        list->value_ = std::static_pointer_cast<Value>(
                std::make_shared<Symbol>(Symbol("quote")));

        list->next_ = std::static_pointer_cast<Value>(
                std::make_shared<Pair>(Pair(BuildSyntaxTree(tokens))));
        return list;
    }

    if (token == "(") {
        auto list = std::make_shared<Pair>(Pair());
        auto currentPair = list;
        while (tokens->front() != ")") {
            currentPair->value_ = BuildSyntaxTree(tokens);
            currentPair->next_ = std::make_shared<Pair>(Pair());

            if (tokens->size() == 0) {
                throw LispSyntaxException("syntax error");
            }

            if (tokens->front() == ".") {
                tokens->pop_front();
                currentPair->next_ = BuildSyntaxTree(tokens);
                break;
            } else {
                currentPair = std::dynamic_pointer_cast<Pair>(
                        currentPair->next_);
            }
        }

        if (tokens->size() == 0) {
            throw LispSyntaxException("syntax error");
        }

        tokens->pop_front();
        return std::static_pointer_cast<Value>(list);
    } else {
        if (IsNumber(&token)) {
            return std::make_shared<Number>(Number(atoi(token.c_str())));
        } else {
            if (token == ".") {
                throw LispSyntaxException("syntax error");
            }
            return std::make_shared<Symbol>(Symbol(token));
        }
    }
}

