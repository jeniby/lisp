#ifndef LISPP_LISPP_UTILS_EXCEPTIONS_H
#define LISPP_LISPP_UTILS_EXCEPTIONS_H

#include <stdexcept>

class LispSyntaxException: public std::logic_error {
public:

    LispSyntaxException(const std::string& message)
            : logic_error(message)
    {}
};

class LispNameException: public std::logic_error {
public:

    LispNameException(const std::string& message)
            : logic_error(message)
    {}
};


class LispRuntimeException: public std::runtime_error {
public:

    LispRuntimeException(const std::string& message)
            : runtime_error(message)
    {}
};

#endif
