#include <lispp/types/procedure.h>
#include "helper.h"
#include <vector>

std::vector<std::shared_ptr<Value>> PairsToVector(
        std::shared_ptr<Pair> pair,

        const std::shared_ptr<Scope>& env,
        bool evaluate) {
    std::vector<std::shared_ptr<Value>> result;

    auto currentPair = pair;

    while (currentPair->value_) {
        if (evaluate) {
            result.push_back(currentPair->value_->Evaluate(env));
        } else {
            result.push_back(currentPair->value_);
        }

        if (IsNumberValue(currentPair->next_)) {
            result.push_back(currentPair->next_);
            break;
        }
        currentPair = std::dynamic_pointer_cast<Pair>(currentPair->next_);
    }
    return result;
}

bool IsNumberValue(std::shared_ptr<Value> obj) {
    return std::dynamic_pointer_cast<Number>(obj) != nullptr;
}

bool IsSymbolValue(std::shared_ptr<Value> obj) {
    return std::dynamic_pointer_cast<Symbol>(obj) != nullptr;
}

bool IsPairValue(std::shared_ptr<Value> obj) {
    return std::dynamic_pointer_cast<Pair>(obj) != nullptr;
}

bool IsFunctionValue(std::shared_ptr<Value> obj) {
    return std::dynamic_pointer_cast<Function>(obj) != nullptr;
}

bool IsNilValue(std::shared_ptr<Value> obj) {
    return std::dynamic_pointer_cast<Nil>(obj) != nullptr;
}

bool IsTrueValue(std::shared_ptr<Value> obj) {
    return std::dynamic_pointer_cast<True>(obj) != nullptr;
}

bool IsFalseValue(std::shared_ptr<Value> obj) {
    return std::dynamic_pointer_cast<False>(obj) != nullptr;
}
