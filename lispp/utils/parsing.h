#ifndef LISPP_LISPP_UTILS_PARSING_H_
#define LISPP_LISPP_UTILS_PARSING_H_

#include <list>
#include <memory>
#include <string>

std::list<std::string> Split(const std::string & input);

bool IsNumber(std::string* token);

std::shared_ptr<Value> BuildSyntaxTree(std::list<std::string>* tokens);

#endif