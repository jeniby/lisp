#ifndef LISPP_LISPP_UTILS_HELPER_H_
#define LISPP_LISPP_UTILS_HELPER_H_


#include <vector>
#include <memory>
#include <lispp/types/simple_type.h>
#include "lispp/types/pair.h"

std::vector<std::shared_ptr<Value>> PairsToVector(
        std::shared_ptr<Pair> pair,
        const std::shared_ptr<Scope>& env,
        bool evaluate);

bool IsNumberValue(std::shared_ptr<Value> obj);

bool IsSymbolValue(std::shared_ptr<Value> obj);

bool IsPairValue(std::shared_ptr<Value> obj);

bool IsFunctionValue(std::shared_ptr<Value> obj);

bool IsNilValue(std::shared_ptr<Value> obj);

bool IsTrueValue(std::shared_ptr<Value> obj);

bool IsFalseValue(std::shared_ptr<Value> obj);
#endif
