#include <lispp/types/simple_type.h>
#include <iostream>
#include "interpreter.h"
#include "lispp/utils/parsing.h"
#include "lispp/utils/helper.h"
#include "lispp/utils/exceptions.h"


std::string Interpreter::Run(const std::string& expression) {
    std::list<std::string> in = Split(expression);

    auto ast = BuildSyntaxTree(&in);

    if (in.size() != 0) {
        throw LispSyntaxException("syntax error");
    }

    auto result = ast->Evaluate(env_)->ToString();
    return result;
}

Interpreter::~Interpreter() {
    if (env_->env_.size() != 0) {
        env_->env_.clear();
    }
}

Interpreter::Interpreter() {
    env_ = std::make_shared<Scope>();
    env_->SetGlobalOperations();
}
